import { DashBoardPage } from './app.po';

describe('dash-board App', function() {
  let page: DashBoardPage;

  beforeEach(() => {
    page = new DashBoardPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
